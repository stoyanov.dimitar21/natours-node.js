# Documentation
https://documenter.getpostman.com/view/11815367/2s93eX2ZJr

## Import data
#### Need to comment middleware pre save in user model before importing
node .\dev-data\data\import-dev-data.js --import

## Delete data
node .\dev-data\data\import-dev-data.js --delete

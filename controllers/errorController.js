const AppError = require('./../utils/appError')

const handleCastErrorDB = err => {
    const message = `Invalid: ${err.path}: ${err.value}`;
    return new AppError(message, 400);
}

const handleDuplicateFieldsDB = err => {
    const message = `Duplicate field value. Please use another value!`;
    return new AppError(message, 400);
}

const handleValidationErrorDB = err => {
    const errors = Object.values(err.errors).map(el => el.message)

    const message = `Invalid input data. ${errors.join('. ')}`;
    return new AppError(message, 400);
}

const sendErrorDev = (err, res) => {
    res.status(err.statusCode).json({
        status: err.status,
        message: err.message,
        error: err,
        stack: err.stack
    });
}

const sendErrorProd = (err, res) => {
    if (err.isOperation) {
        res.status(err.statusCode).json({
            status: err.status,
            message: err.message
        });
    } else {
        console.error(err);
        res.status(500).json({
            status: 'error',
            message: 'Something went wrong'
        });
    }
}

module.exports = (err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error';

    const env = process.env.NODE_ENV;
    if (env === 'development') {
        sendErrorDev(err, res);
    }
    if (env === 'production') {
        const error = { ...err };

        if (error.name === 'CastError')       error = handleCastErrorDB(error);
        if (error.code === 11000)             error = handleDuplicateFieldsDB(error);
        if (error.code === 'ValidationError') error = handleValidationErrorDB(error);

        sendErrorApp(error, res);
    } 
}